%global		xmmsinputplugindir	%(xmms-config --input-plugin-dir 2>/dev/null)

Summary:	Library and frontend for decoding MPEG2/4 AAC
Name:		faad2
Epoch:		1
Version:	2.8.8
Release:	2
License:	GPLv2+
Group:		Applications/Multimedia
URL:		http://www.audiocoding.com/faad2.html
Source:		http://downloads.sourceforge.net/sourceforge/faac/%{name}-%{version}.tar.gz
Patch0:		faad2-pic.patch			
Patch1:		fix_undefined_version.patch

BuildRequires:	gcc-c++
BuildRequires:	automake
BuildRequires:	libtool
BuildRequires:	id3lib-devel
BuildRequires:	libsysfs-devel
BuildRequires:	xmms-devel
BuildRequires:	zlib-devel

%description
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely
written from scratch.

%package libs
Summary:	Shared libraries of the FAAD 2 AAC decoder
Group:		System Environment/Libraries
Obsoletes:	%{name} < 1:2.6.1-3

%description libs
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely
written from scratch.

This package contains libfaad.

%package devel
Summary:	Development libraries of the FAAD 2 AAC decoder
Group:		Development/Libraries
Requires:	%{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}

%description devel
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely
written from scratch.

This package contains development files and documentation for libfaad.

%package -n xmms-%{name}
Summary:	AAC XMMS Input Plugin
Group:		Applications/Multimedia
Requires:	%{name}-libs%{?_isa} = %{epoch}:%{version}-%{release}
Provides:	xmms-aac%{?_isa} = %{version}-%{release}
Obsoletes:	xmms-aac < 2.6.1

%description -n xmms-%{name}
FAAD 2 is a LC, MAIN and LTP profile, MPEG2 and MPEG-4 AAC decoder, completely
written from scratch.

This package contains an input plugin for xmms.

%prep
%autosetup -p1
autoreconf -fiv

%build
%configure \
    --disable-static \
    --with-xmms

%make_build

%install
%make_install

#Remove libtool archives.
find $RPM_BUILD_ROOT -name '*.la' -or -name '*.a' | xargs rm -f


%ldconfig_scriptlets libs


%files
%doc AUTHORS ChangeLog NEWS README*
%license COPYING
%{_bindir}/faad
%{_mandir}/man1/faad.1*

%files libs
%{_libdir}/libfaad*.so.*

%files devel
%doc TODO
%{_includedir}/faad.h
%{_includedir}/neaacdec.h
%{_libdir}/libfaad*.so

%files -n xmms-%{name}
%doc plugins/xmms/{AUTHORS,NEWS,ChangeLog,README,TODO}
%{xmmsinputplugindir}/libmp4.so

%changelog
* Fri Jul 01 2022 zuohongsheng <zuohongsheng@kylinos.cn> - 1:2.8.8-2
- add faad2-pic.patch. PIC(position independent code) allow shared libraries to be loaded anywhere in the address space.
- fix_undefined_version.patch. set FAAD2_VERSION "2.8.8".
 
* Tue Jun 28 2022 zuohongsheng <zuohongsheng@kylinos.cn> - 1:2.8.8-1
- Package init

